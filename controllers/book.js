const Book = require('../models/book');
const fs = require('fs');
const readline = require('readline');
const createError = require('http-errors');

exports.importData = async (req, res, next) => {
    const filePath = req.file.path;
    const rl = readline.createInterface({
        input: fs.createReadStream(filePath),
        output: process.stdout,
        terminal: false
    });

    const records = [];

    rl.on('line', (line) => {
        const [code, author, title, year, pages, price] = line.split(',');
        records.push({
            code,
            author,
            title,
            year: parseInt(year),
            pages: parseInt(pages),
            price: parseFloat(price)
        });
    });

    rl.on('close', async () => {
        try {
            await Book.insertMany(records);
            res.send('Data imported successfully');
        } catch (err) {
            next(createError(500, err.message));
        }
    });
};

exports.getTotalPrice = async (req, res, next) => {
    try {
        const totalPrice = await Book.aggregate([
            { $group: { 
                _id: null, 
                total_price: { 
                    $sum: { 
                        $convert: { 
                            input: "$price", 
                            to: "int", 
                            onError: 0, 
                            onNull: 0 
                        } 
                    } 
                } 
            }  }
        ]);
        res.send(`Total price of all books: ${totalPrice[0].total_price}`);
    } catch (err) {
        next(createError(500, err.message));
    }
};
