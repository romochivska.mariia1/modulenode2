const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const bookController = require('../controllers/book');

router.post('/import', upload.single('file'), bookController.importData);     // TASK 1  Потрібно завантажувати CSV файл.
router.get('/page-count', bookController.getTotalPrice);            

module.exports = router;
