const mongoose = require('mongoose');

const BookSchema = new mongoose.Schema({
    code: String,
    author: String,
    title: String,
    year: String,
    pages: String,
    price: String
});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;
