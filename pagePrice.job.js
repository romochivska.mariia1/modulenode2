const CronJob = require('cron').CronJob;
const axios = require('axios');
const bookController = require('./controllers/book');

function startPagePriceJob() {
    const job = new CronJob(
        '*/30 * * * * *', 
        async () => {
            try {
                const response = await axios.get('http://localhost:3007/book/page-count');
                console.log(`[pagePrice.job] Total price: ${response.data}`);
            } catch (error) {
                console.error(`[pagePrice.job] Error: ${error.message}`);
            }
        },
    );

    job.start();
}

module.exports = startPagePriceJob;
