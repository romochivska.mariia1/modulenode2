const Book = require('../models/book');
const fs = require('fs');
const readline = require('readline');

const importData = (filePath) => {
    return new Promise((resolve, reject) => {
        const rl = readline.createInterface({
            input: fs.createReadStream(filePath),
            output: process.stdout,
            terminal: false
        });

        const records = [];

        rl.on('line', (line) => {
            const [code, author, title, year, pages, price] = line.split(','); 
            records.push({
                code,
                author,
                title,
                year: parseInt(year), 
                pages: parseInt(pages), 
                price: parseFloat(price) 
            });
        });

        rl.on('close', async () => {
            try {
                await Book.insertMany(records);
                resolve('Data imported successfully');
            } catch (err) {
                reject(err.message);
            }
        });
    });
};

const getTotalPrice = async () => {
    const totalPrice = await Book.aggregate([
        { $group: { _id: null, total_price: { $sum: "$price" } } }
    ]);
    return totalPrice[0].total_price;
};

module.exports = {
    importData,
    getTotalPrice
};
